//---------------------------------------------------------------------------

#pragma once

#ifndef APP_WIDGETS_LINE_H
#define APP_WIDGETS_LINE_H

//---------------------------------------------------------------------------

#include <app/widgets/drawing.h>
#include <ui/layers/line_layer.h>
#include <ui/models/stacking_model.h>

#include <color/preset.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace s
        {
            define_symbol(point_type);
        }

        namespace widgets
        {
            template <class PointType>
            class line : public app::widgets::drawing
            {
            public:
                using point_type = PointType;
                
                struct c : widgets::drawing::c
                {
                    static constexpr meta::type<ui::line_layer<geom::line_mesh<point_type, geom::primitives_type::triangles>>> line{};
                    static constexpr meta::type<ui::line_layer<geom::line_mesh<point_type, geom::primitives_type::lines>>> framed_line{};
                };

                static constexpr auto model = ui::stack(
                    c::line,
                    c::framed_line
                );

                line(ui::entity_handle && model) :
                    app::widgets::drawing(std::move(model))
                {
                    _model[c::line].color = color::fade(color::black, 1.0f);
                    _model[c::framed_line].color = color::fade(color::white, 0.05f);
                }

                line(line &&) noexcept = default;

                bool append(const auto & pos, float thickness, auto join, auto cap) {
                    using model_type = plaintype(model)::type;

                    return meta::accumulate(model_type::submodels, meta::empty, [&](auto is_allowed, auto type) {
                        auto & line_model = _model[type];

                        if constexpr (meta::is_empty(is_allowed)) {
                            if (!line_model.line.can_place(pos, thickness)) {
                                return false;
                            }
                        } else {
                            if (!is_allowed) {
                                return false;
                            }
                        }
                            
                        line_model.line.append(&pos, &pos + 1, thickness, join, cap);
                        ui::mark_updated(_model);

                        return true;
                    });
                }
            };
        }

        static constexpr struct
        {
            template <class... T> requires(
                symbol::matches<T...> (
                    s::context      [symbol::required][symbol::unique],
                    s::point_type   [symbol::required][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                using point_type = plain<symbol::named_type<s::type::point_type, T...>>;
                using widget_type = widgets::line<point_type>;
                static_assert(std::is_base_of_v<widgets::drawing, widget_type>, "Unsupported line type");

                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                return widget_type(
                    named[s::context].create_model(meta::type_v<widget_type>)
                );
            }

        } line;
    }
}

//---------------------------------------------------------------------------
#endif
