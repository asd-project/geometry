//---------------------------------------------------------------------------

#pragma once

#ifndef APP_WIDGETS_CANVAS_H
#define APP_WIDGETS_CANVAS_H

//---------------------------------------------------------------------------

#include <ui/controllers/bound_controller.h>
#include <ui/traits/interactive.h>

#include <app/widgets/drawing.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace widgets
        {
            class canvas :
                public ui::traits::interactive<canvas>
            {
            public:
                canvas() noexcept = default;
                canvas(canvas &&) noexcept = default;
                canvas & operator = (canvas &&) noexcept = default;

                template <class T>
                auto & add(T && drawing) {
                    using Drawing = plain<T>;
                    return static_cast<Drawing &>(*_drawings.emplace_back(std::make_unique<Drawing>(std::forward<T>(drawing))));
                }
                
                const ui::geometry & geometry() const noexcept {
                    return _geometry;
                }

                void update(const ui::geometry & geometry) noexcept {
                    if (_geometry == geometry) {
                        return;
                    }

                    _geometry = geometry;

                    for (auto & drawing : _drawings) {
                        drawing->update(geometry);
                    }
                }

                void hit(ui::trace & trace, const space::point & pos) noexcept {
                    trace.push_back(this->_controller.get());

                    for (auto & drawing : _drawings) {
                        drawing->hit(trace, pos);
                    }
                }

                void clear() {
                    _drawings.clear();
                }

            private:
                ui::geometry _geometry;
                std::vector<std::unique_ptr<drawing>> _drawings;
            };
        }

        static constexpr struct
        {
            template <class... T> requires(
                symbol::matches<T...> (
                    s::controller [symbol::required][symbol::unique]
                )
            )
            auto operator()(T &&... options) const noexcept {
                auto [named, unnamed] = symbol::collect(std::forward<T>(options)...);

                using widget_type = widgets::canvas;
                constexpr bool is_callable = std::is_invocable_v<symbol::named_type<s::type::controller, T...>, widget_type &>;

                widget_type canvas;

                if constexpr (is_callable) {
                    canvas.set_controller(named[s::controller](canvas));
                } else {
                    canvas.set_controller(std::move(named[s::controller]));
                }

                return canvas;
            }

        } canvas;
    }
}

//---------------------------------------------------------------------------
#endif
