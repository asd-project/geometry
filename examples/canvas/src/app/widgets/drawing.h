//---------------------------------------------------------------------------

#pragma once

#ifndef APP_WIDGETS_DRAWING_H
#define APP_WIDGETS_DRAWING_H

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>

#include <ui/model.h>
#include <ui/renderer.h>

#include <app/symbols.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace widgets
        {
            class drawing : public ui::widgets::model_widget
            {
            public:
                using ui::widgets::model_widget::model_widget;
            };
        }
    }
}

//---------------------------------------------------------------------------
#endif
