//---------------------------------------------------------------------------

#pragma once

#ifndef APP_CONTROLLERS_DRAW_CONTROLLER_H
#define APP_CONTROLLERS_DRAW_CONTROLLER_H

//---------------------------------------------------------------------------

#include <ui/controllers/bound_controller.h>
#include <app/widgets/drawing.h>
#include <geometry/line_mesh.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        using line_join = std::variant<geom::line_join_miter, geom::line_join_bevel>;
        using line_cap = std::variant<geom::line_cap_round, geom::line_cap_flat>;

        namespace controllers
        {
            template <class T, class Factory>
            class draw_controller : public ui::bound_controller<T>
            {
            public:
                using widget_type = plaintype(std::declval<Factory>()());

                draw_controller(Factory && factory) : _factory(std::forward<Factory>(factory)) {}

                void set_thickness(float thickness) {
                    _thickness = thickness;
                }

                void set_line_join(const line_join & join) {
                    _join = join;
                }

                void set_line_cap(const line_cap & cap) {
                    _cap = cap;
                }

                void press(const space::point & pos) noexcept override final {
                    _current_drawing = &this->_target->add(_factory());
                    _prev_pos = pos;
                    
                    _current_drawing->append(pos, _thickness, _join, _cap);
                }

                void release() noexcept override final {
                    if (!_current_drawing) {
                        return;
                    }

                    _current_drawing = nullptr;
                }

                bool move(const space::point & pos) noexcept override final {
                    if (!_current_drawing) {
                        return false;
                    }

                    if (_current_drawing->append(pos, _thickness, _join, _cap)) {
                        _prev_pos = pos;
                    }

                    return true;
                }

            private:
                widget_type * _current_drawing = nullptr;
                space::point _prev_pos;
                float _thickness = 4.0f;
                line_join _join = geom::line_join_miter{};
                line_cap _cap = geom::line_cap_round{};
                Factory _factory;
            };
        }

        static constexpr struct
        {
            auto operator() (auto && factory) const {
                using Factory = decltype(factory);

                return [factory{std::forward<Factory>(factory)}](auto & canvas) {
                    using Canvas = plaintype(canvas);

                    return std::make_unique<app::controllers::draw_controller<Canvas, plain<Factory>>>(std::forward<Factory>(factory));
                };
            }

            auto operator() (auto && factory, auto && config_callback) const {
                using Factory = decltype(factory);
                using Callback = decltype(config_callback);

                return [&factory, &config_callback](auto & canvas) {
                    using Canvas = plaintype(canvas);

                    auto controller = std::make_unique<app::controllers::draw_controller<Canvas, plain<Factory>>>(std::forward<Factory>(factory));
                    std::forward<Callback>(config_callback)(*controller);

                    return controller;
                };
            }
        } draw_controller;
    }
}

//---------------------------------------------------------------------------
#endif
