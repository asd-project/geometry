//---------------------------------------------------------------------------

#pragma once

#ifndef APP_SYMBOLS_H
#define APP_SYMBOLS_H

//---------------------------------------------------------------------------

#include <ui/symbols.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        using namespace meta::literals;

        namespace s
        {
            using namespace ui::s;

            namespace type
            {
                using namespace ui::s::type;
            }

            define_symbol(on_clear);
        }
    }
}

//---------------------------------------------------------------------------
#endif
