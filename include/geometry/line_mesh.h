//---------------------------------------------------------------------------

#pragma once

#ifndef GEOMETRY_LINE_MESH_H
#define GEOMETRY_LINE_MESH_H

//---------------------------------------------------------------------------

#include <geometry/mesh.h>
#include <geometry/path.h>

//---------------------------------------------------------------------------

namespace asd::geometry
{
    template <class PointType>
    struct line_vertex
    {
        using point_type = PointType;

        point_type position;
        math::float_point_xyz texcoord;
    };

    static_assert(std::is_standard_layout_v<line_vertex<math::float_point>>);
    static_assert(sizeof(line_vertex<math::float_point>) == 20);
    static_assert(sizeof(line_vertex<math::float_point_xyz>) == 24);

    template <class PointType>
    struct line_anchor
    {
        using point_type = PointType;
        using distance_type = float;

        point_type position;
        float offset = 0.0f;
        u32 vertices_offset = 0;
        u32 indices_offset = 0;
    };

    template <class PointType, geom::primitives_type primitives_type>
    struct line_mesh_builder
    {
        using anchor_type = line_anchor<PointType>;
        using vertex_type = line_vertex<PointType>;
        using point_type = typename anchor_type::point_type;

        void vertex(const point_type & position, const math::float_point_xyz & texcoord) {
            vertices.emplace_back(position, texcoord);
        }

        void triangle(const anchor_type & anchor, u32 a, u32 b, u32 c) {
            if constexpr (primitives_type == geom::primitives_type::lines) {
                indices.push_back(anchor.vertices_offset + a);
                indices.push_back(anchor.vertices_offset + b);
                indices.push_back(anchor.vertices_offset + b);
                indices.push_back(anchor.vertices_offset + c);
                indices.push_back(anchor.vertices_offset + a);
                indices.push_back(anchor.vertices_offset + c);
            } else {
                indices.push_back(anchor.vertices_offset + a);
                indices.push_back(anchor.vertices_offset + b);
                indices.push_back(anchor.vertices_offset + c);
            }
        }

        std::vector<vertex_type> vertices;
        std::vector<u32> indices;
    };

    struct line_join_miter {};
    struct line_join_bevel {};

    // struct line_join_round
    // {
    //     constexpr line_join_round(float division_coeff = 2.0f) :
    //         division_coeff(division_coeff) {}

    //     /**
    //      * @brief Allows to tweak join arc quality
    //      * Actual amount of points depends on the arc size
    //      * radius = 0.5 * thickness
    //      * divisions = math::ceil(2.0 * division_coeff * radius * angle / math::pi)
    //      */
    //     float division_coeff;
    // };

    struct line_cap_flat {};
    struct line_cap_square {};

    struct line_cap_round
    {
        constexpr line_cap_round(float division_coeff = 2.0f) :
            division_coeff(division_coeff) {}

        /**
         * @brief Allows to tweak cap arc quality
         * Actual amount of points depends on the arc size
         * radius = 0.5 * thickness
         * divisions = math::ceil(2.0 * division_coeff * radius)
         */
        float division_coeff;
    };

//---------------------------------------------------------------------------

    template <class Output>
    void line_point(line_cap_flat cap, Output & output, typename Output::anchor_type & anchor, float thickness);

    template <class Output>
    void start_cap(line_cap_flat, Output & output, typename Output::anchor_type & anchor, const typename Output::anchor_type & to, float thickness);

    template <class Output>
    void end_cap(line_cap_flat, Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & v, float start_offset, float thickness);

    template <class Output>
    void line_point(const line_cap_round & cap, Output & output, typename Output::anchor_type & anchor, float thickness);

    template <class Output>
    void start_cap(const line_cap_round & cap, Output & output, typename Output::anchor_type & anchor, const typename Output::anchor_type & to, float thickness);

    template <class Output>
    void end_cap(const line_cap_round & cap, Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & v, float start_offset, float thickness);

    template <class Output, class ... V>
    void line_point(const std::variant<V...> & cap, Output & output, typename Output::anchor_type & anchor, float thickness) {
        std::visit([&](const auto & cap) { line_point(cap, output, anchor, thickness); }, cap);
    }

    template <class Output, class ... V>
    void start_cap(const std::variant<V...> & cap, Output & output, typename Output::anchor_type & anchor, const typename Output::anchor_type & to, float thickness) {
        std::visit([&](const auto & cap) { start_cap(cap, output, anchor, to, thickness); }, cap);
    }

    template <class Output, class ... V>
    void end_cap(const std::variant<V...> & cap, Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & v, float start_offset, float thickness) {
        std::visit([&](const auto & cap) { end_cap(cap, output, anchor, v, start_offset, thickness); }, cap);
    }

//---------------------------------------------------------------------------

    template <class Output, class It>
    void append_line(line_join_miter, Output & output, It it, It end, float thickness);

    template <class Output, class It>
    void append_line(line_join_bevel, Output & output, It it, It end, float thickness);

    // template <class Output, class It>
    // void append_line(const line_join_round & join, Output & output, It it, It end, float thickness);

    template <class Output, class It, class ... V>
    void append_line(const std::variant<V...> & join, Output & output, It begin, It end, float thickness) {
        std::visit([&](const auto & join) { append_line(join, output, begin, end, thickness); }, join);
    }

//---------------------------------------------------------------------------

    template <
        class PointType,
        geom::primitives_type PrimitivesType = geom::primitives_type::triangles,
        class SmoothingStrategy = geometry::no_smoothing_strategy<>
    >
    class line_mesh
    {
    public:
        static constexpr auto primitives_type = PrimitivesType;

        using builder_type = line_mesh_builder<PointType, primitives_type>;
        using point_type = typename builder_type::point_type;
        using line_anchor_type = typename builder_type::anchor_type;

        const auto & vertices() const noexcept {
            return _output.vertices;
        }

        const auto & indices() const noexcept {
            return _output.indices;
        }

        bool can_place(const point_type & pt, float thickness) {
            if (_path.empty()) {
                return true;
            }

            if (math::square_distance(pt, _path.back().position) < thickness * thickness) {
                return false;
            }
                
            return true;
        }

        template <class It, class Join = line_join_miter, class Cap = line_cap_round>
        void append(It first, It last, float thickness, const Join & join = Join{}, const Cap & cap = Cap{}) {
            if (first == last) {
                return;
            }

            auto previous_size = _path.size();
            _smoothing.insert(_path, _path.end(), first, last);

            if (_path.size() == 1) {
                line_point(cap, _output, _path[0], thickness);
                return;
            }

            if (previous_size == 0) {
                start_cap(cap, _output, _path[0], _path[1], thickness);
                previous_size = 1;
            } else if (previous_size == 1) {
                _output.vertices.clear();
                _output.indices.clear();
                start_cap(cap, _output, _path[0], _path[1], thickness);
            } else {
                // fix last point
                line_anchor_type & last_anchor = _path[previous_size - 1];
                _output.vertices.erase(_output.vertices.begin() + last_anchor.vertices_offset, _output.vertices.end());
                _output.indices.erase(_output.indices.begin() + last_anchor.indices_offset, _output.indices.end());
            }

            if (previous_size >= 2) {
                append_line(join, _output, _path.begin() + previous_size - 2, _path.end(), thickness);
            }

            auto & last_anchor = _path[_path.size() - 1];
            auto & prev_anchor = _path[_path.size() - 2];
            end_cap(cap, _output, last_anchor, last_anchor.position - prev_anchor.position, prev_anchor.offset, thickness);
        }

    private:
        std::vector<line_anchor_type> _path;
        builder_type _output;
        SmoothingStrategy _smoothing;
    };

//---------------------------------------------------------------------------

    template <class Output>
    void append_section(Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & normal, float offset, float thickness) {
        anchor.vertices_offset = output.vertices.size();
        anchor.indices_offset = output.indices.size();
        anchor.offset = offset;

        auto n = thickness * normal;

        output.vertex(anchor.position + n, {offset, +1.0f, thickness});
        output.vertex(anchor.position - n, {offset, -1.0f, thickness});

        output.triangle(anchor, -2, +1, -1);
        output.triangle(anchor, -2, +0, +1);
    }

    template <class Output>
    void append_bevel(Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & a, const typename Output::point_type & b, float dot, float offset, float thickness) {
        anchor.vertices_offset = output.vertices.size();
        anchor.indices_offset = output.indices.size();
        anchor.offset = offset;

        auto n = thickness * math::tangent_normal(a, -b);
        auto extension_length = thickness / math::fast::sqrt(1.0f - dot * dot); // calculate triangle hypotenuse

        if (math::signed_angle(a, b) >= 0) { // TODO: optimization opportunity
            output.vertex(anchor.position + b * extension_length, {offset, +1.0f, thickness});
            output.vertex(anchor.position + a * extension_length, {offset, +1.0f, thickness});
            output.vertex(anchor.position - n, {offset, -1.0f, thickness});

            // prev to bevel
            output.triangle(anchor, -2, +2, -1);
            output.triangle(anchor, -2, +0, +2);

            // bevel
            output.triangle(anchor, +0, +1, +2);
        } else {
            output.vertex(anchor.position + b * extension_length, {offset, -1.0f, thickness});
            output.vertex(anchor.position + n, {offset, +1.0f, thickness});
            output.vertex(anchor.position + a * extension_length, {offset, -1.0f, thickness});

            // prev to bevel
            output.triangle(anchor, -2, +0, -1);
            output.triangle(anchor, -2, +1, +0);

            // bevel
            output.triangle(anchor, +0, +1, +2);
        }
    }
    
    template <class Output>
    void line_point(line_cap_flat cap, Output & output, typename Output::anchor_type & anchor, float thickness) {
        //
    }
    
    template <class Output>
    void start_cap(line_cap_flat, Output & output, typename Output::anchor_type & anchor, const typename Output::anchor_type & to, float thickness) {
        auto normal = thickness * math::line_normal(math::normalize(to.position - anchor.position));

        output.vertex(anchor.position + normal, {0.0f, +1.0f, thickness});
        output.vertex(anchor.position - normal, {0.0f, -1.0f, thickness});
    }
    
    template <class Output>
    void end_cap(line_cap_flat, Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & v, float start_offset, float thickness) {
        auto normal = math::line_normal(math::normalize(v));
        append_section(output, anchor, normal, start_offset + math::length(v), thickness);
    }
    
    template <class Output>
    void line_point(const line_cap_round & cap, Output & output, typename Output::anchor_type & anchor, float thickness) {
        anchor.vertices_offset = output.vertices.size();
        anchor.indices_offset = output.indices.size();

        output.vertex(anchor.position, {0.0f, 0.0f, thickness});

        auto divisions = math::ceil<int>(2.0f * cap.division_coeff * thickness);
        output.vertex(anchor.position + typename Output::point_type{thickness, 0}, {0.0f, 1.0f, thickness});

        for (int i = 2; i <= divisions; ++i) {
            auto angle = 2.0f * math::pi * i / divisions;
            output.vertex(anchor.position + thickness * math::fast::direction(angle), {0.0f, 1.0f, thickness});

            output.triangle(anchor, 0, i, i - 1);
        }

        output.triangle(anchor, 0, 1, divisions);
    }
    
    template <class Output>
    void start_cap(const line_cap_round & cap, Output & output, typename Output::anchor_type & anchor, const typename Output::anchor_type & to, float thickness) {
        auto normal = math::line_normal(math::fast::normalize(to.position - anchor.position));
        auto antinormal = -normal;
        auto middle = math::avg(anchor.position, to.position); // required for transition from the 3-point segment to the 2-point (to avoid junction at the line cap)
        auto middle_distance = math::distance(anchor.position, middle);

        output.vertex(anchor.position + thickness * normal, {0.0f, 1.0f, thickness});

        auto divisions = math::ceil<int>(cap.division_coeff * thickness);

        for (int i = 0; i < divisions; ++i) {
            output.vertex(anchor.position + thickness * math::fast::rotate(normal, math::pi * float(i + 1) / (divisions + 1)), {0.0f, 1.0f, thickness});
            output.triangle(anchor, divisions + 1, i + 1, i);
        }

        output.triangle(anchor, divisions + 1, divisions + 2, divisions);

        output.vertex(anchor.position, {0.0f, 0.0f, thickness});
        output.vertex(anchor.position + thickness * antinormal, {0.0f, 1.0f, thickness});
        output.vertex(anchor.position + thickness * antinormal, {0.0f, -1.0f, thickness});

        output.vertex(middle + thickness * normal, {middle_distance, +1.0f, thickness});
        output.vertex(middle + thickness * antinormal, {middle_distance, -1.0f, thickness});

        output.triangle(anchor, divisions + 1, 0, divisions + 4);
        output.triangle(anchor, divisions + 1, divisions + 5, divisions + 3);
        output.triangle(anchor, divisions + 1, divisions + 4, divisions + 5);
    }
    
    template <class Output>
    void end_cap(const line_cap_round & cap, Output & output, typename Output::anchor_type & anchor, const typename Output::point_type & v, float start_offset, float thickness) {
        auto offset = math::length(v);

        anchor.vertices_offset = output.vertices.size();
        anchor.indices_offset = output.indices.size();
        anchor.offset = start_offset + offset;

        auto normal = thickness * math::line_normal(math::fast::normalize(v));
        auto antinormal = -normal;
        auto middle = anchor.position - v * 0.5f; // required for transition from the 2-point segment to the 3-point (to avoid junction at the line cap)

        auto divisions = math::ceil<int>(cap.division_coeff * thickness);

        output.vertex(middle + normal, {start_offset + offset * 0.5f, +1.0f, thickness});        // +0
        output.vertex(middle + antinormal, {start_offset + offset * 0.5f, -1.0f, thickness});    // +1
        output.vertex(anchor.position + normal, {anchor.offset, 1.0f, thickness});                  // +2
        output.vertex(anchor.position, {anchor.offset, 0.0f, thickness});                           // +3
        output.vertex(anchor.position + antinormal, {anchor.offset, -1.0f, thickness});             // +4

// prev to middle
        output.triangle(anchor, -2, +1, -1);
        output.triangle(anchor, -2, +0, +1);

// middle to cap
        output.triangle(anchor, +0, +2, +3);
        output.triangle(anchor, +0, +3, +1);
        output.triangle(anchor, +3, +4, +1);

//  cap
        auto arc_start_index = 4;

        for (int i = 0; i < divisions; ++i) {
            output.vertex(anchor.position + math::fast::rotate(antinormal, math::pi * float(i + 1) / (divisions + 1)), {anchor.offset, -1.0f, thickness});
            output.triangle(anchor, arc_start_index - 1, arc_start_index + i + 1, arc_start_index + i);
        }

        output.vertex(anchor.position - normal, {anchor.offset, +1.0f, thickness});
        output.vertex(anchor.position + normal, {anchor.offset, -1.0f, thickness});

        output.triangle(anchor, arc_start_index - 1, arc_start_index + divisions + 2, arc_start_index + divisions);
    }

    template <class Output, class It>
    void append_line(line_join_miter, Output & output, It it, It end, float thickness) {
        auto prev = it++;

        for (auto next = std::next(it); next != end; prev = std::exchange(it, next++)) {
            auto & point = it->position;
            auto v = point - prev->position;
            auto a = math::normalize(v);
            auto b = math::normalize(point - next->position);
            auto dot = math::dot(a, b);
            auto length = math::length(v);

            if (dot > 0.8f) {
                auto normal = math::line_normal(a);
                append_section(output, *it, normal, prev->offset + length, thickness);
                append_section(output, *it, -normal, prev->offset + length, thickness);
            } else if (dot < -0.9f) {
                append_section(output, *it, math::line_normal(a), prev->offset + length, thickness);
            } else {
                append_section(output, *it, math::tangent_normal(a, -b), prev->offset + length, thickness);
            }
        }
    }
    
    template <class Output, class It>
    void append_line(line_join_bevel, Output & output, It it, It end, float thickness) {
        auto prev = it++;

        for (auto next = std::next(it); next != end; prev = std::exchange(it, next++)) {
            auto & point = it->position;
            auto v = point - prev->position;
            auto a = math::normalize(v);
            auto b = math::normalize(point - next->position);
            auto dot = math::dot(a, b);
            auto length = math::length(v);

            if (dot > 0.8f) {
                auto normal = math::line_normal(a);
                append_section(output, *it, normal, prev->offset + length, thickness);
                append_section(output, *it, -normal, prev->offset + length, thickness);
            } else if (dot < -0.7f) {
                append_section(output, *it, math::line_normal(a), prev->offset + length, thickness);
            } else {
                append_bevel(output, *it, a, b, dot, prev->offset + length, thickness);
            }
        }
    }
    
//     template <class Output, class It>
//     void append_line(const line_join_round & join, Output & output, It it, It end, float thickness) {
//         auto prev = it++;

//         for (auto next = std::next(it); next != end; prev = std::exchange(it, next++)) {
//             auto & anchor = *it;
//             auto v = anchor.position - prev->position;
//             auto a = math::normalize(v);
//             auto b = math::normalize(anchor.position - next->position);
//             auto angle = math::signed_angle(a, b);
//             auto abs_angle = math::abs(angle);
//             auto normal = math::line_normal(a);

//             if (abs_angle >= math::pi * 0.95) {
//                 append_section(output, *it, normal, prev->offset + math::length(v), thickness);
//             } else {
//                 anchor.vertices_offset = output.vertices.size();
//                 anchor.indices_offset = output.indices.size();
//                 anchor.offset = prev->offset + math::length(v);

//                 auto divisions = math::ceil<int>(join.division_coeff * thickness * abs_angle / math::pi);
//                 auto n = math::tangent_normal(a, -b);

//                 if (angle > 0.0f) {
//                     output.vertex(anchor.position + n * thickness, {anchor.offset, -1.0f, thickness});   // +0
//                     output.vertex(anchor.position, {anchor.offset, 0.0f, thickness});                    // +1

// // prev to arc start
//                     output.indices.push_back(anchor.vertices_offset - 2);
//                     output.indices.push_back(anchor.vertices_offset + 2); // arc start
//                     output.indices.push_back(anchor.vertices_offset - 1);

//                     output.indices.push_back(anchor.vertices_offset - 2);
//                     output.indices.push_back(anchor.vertices_offset + 0);
//                     output.indices.push_back(anchor.vertices_offset + 2); // arc start

// // arc side triangle
//                     output.indices.push_back(anchor.vertices_offset + 0);
//                     output.indices.push_back(anchor.vertices_offset + 1);
//                     output.indices.push_back(anchor.vertices_offset + 2); // arc start

//                     auto arc_start_index = anchor.vertices_offset + 2;

//                     for (int i = 0; i < divisions; ++i) {
//                         output.vertex(anchor.position + math::fast::rotate(normal, angle * float(i + 1) / (divisions + 1)), {anchor.offset, +1.0f, thickness});

//                         output.indices.push_back(arc_start_index - 1);
//                         output.indices.push_back(arc_start_index + i + 1);
//                         output.indices.push_back(arc_start_index + i);
//                     }

//                     output.indices.push_back(arc_start_index - 1);
//                     output.indices.push_back(arc_start_index + divisions + 2);
//                     output.indices.push_back(arc_start_index + divisions);
//                 }
//             }
//         }
//     }
}

//---------------------------------------------------------------------------
#endif
