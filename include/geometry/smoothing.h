//---------------------------------------------------------------------------

#pragma once

#ifndef GEOMETRY_SMOOTHING_H
#define GEOMETRY_SMOOTHING_H

//---------------------------------------------------------------------------

#include <math/point.h>

//---------------------------------------------------------------------------

namespace asd::geometry
{
    template <class InputIterator, class OutputIterator>
    void smooth_spline_path(InputIterator begin, InputIterator middle, InputIterator end, OutputIterator output, float step) {

    }
}

//---------------------------------------------------------------------------
#endif
