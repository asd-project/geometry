//---------------------------------------------------------------------------

#pragma once

#ifndef GEOMETRY_MESH_H
#define GEOMETRY_MESH_H

//---------------------------------------------------------------------------

namespace asd
{
    namespace geometry
    { 
        enum class primitives_type
        {
            triangles,
            lines
        };
    }

    namespace geom = geometry;
}

//---------------------------------------------------------------------------
#endif
