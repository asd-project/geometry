//---------------------------------------------------------------------------

#pragma once

#ifndef GEOMETRY_PATH_H
#define GEOMETRY_PATH_H

//---------------------------------------------------------------------------

#include <math/point.h>
#include <math/range.h>

#include <vector>
#include <iterator>

#include <boost/assert.hpp>

//---------------------------------------------------------------------------

namespace asd::geometry
{
    namespace wrapping
    {
        struct clamp;
    };

    template <class Wrapping = wrapping::clamp>
    struct no_smoothing_strategy;

    template <class T>
    concept path_anchor = requires (T v) {
        typename T::point_type;
        typename T::distance_type;

        requires std::constructible_from<T, typename T::point_type, typename T::distance_type>;

        { v.position } -> std::convertible_to<typename T::point_type>;
        { v.offset } -> std::convertible_to<typename T::distance_type>;
    };

    template <class PointType>
    struct default_anchor
    {
        using point_type = PointType;
        using distance_type = math::fp<typename PointType::coord_type>;
            
        constexpr default_anchor() = default;
        constexpr default_anchor(const default_anchor &) = default;
        constexpr default_anchor(const PointType & position, distance_type offset) noexcept :
            position(position), offset(offset) {}

        constexpr default_anchor & operator = (const default_anchor &) = default;

        PointType position;
        distance_type offset{0};
        size_t id = size_t(-1);
    };
        
//---------------------------------------------------------------------------

    template <class PointType, size_t N, class SmoothingStrategy = no_smoothing_strategy<>>
    constexpr auto make_path(const std::array<PointType, N> & points, SmoothingStrategy && sm = {}) noexcept {
        using anchor_type = default_anchor<PointType>;

        std::array<anchor_type, N> path;
        sm.fill(points.begin(), points.end(), path.begin());

        return path;
    }
    
    template <class PointType, class SmoothingStrategy = no_smoothing_strategy<>>
    constexpr auto make_path(const std::vector<PointType> & points, SmoothingStrategy && sm = {}) noexcept {
        using anchor_type = default_anchor<PointType>;

        std::vector<anchor_type> path;
        path.reserve(points.size());

        sm.fill(points.begin(), points.end(), std::back_inserter(path));

        return path;
    }

//---------------------------------------------------------------------------
    
    namespace wrapping
    {
        struct clamp
        {
            template <class InputIterator>
            static constexpr auto length(InputIterator begin, InputIterator end) {
                return std::prev(end)->offset;
            }

            template <class InputIterator>
            static constexpr auto before(InputIterator begin, InputIterator end, auto t) {
                return begin->position;
            }

            template <class InputIterator>
            static constexpr auto after(InputIterator begin, InputIterator end, auto t) {
                return std::prev(end)->position;
            }

            template <class InputIterator, class OutputIterator>
            static constexpr auto select_before(InputIterator begin, InputIterator end, auto from, auto to, OutputIterator output) {
                //
            }

            template <class InputIterator, class OutputIterator>
            static constexpr auto select_after(InputIterator begin, InputIterator end, auto from, auto to, OutputIterator output) {
                //
            }

            template <class InputIterator, class OutputIterator>
            static constexpr auto sample_before(InputIterator begin, InputIterator end, auto from, auto to, auto step, OutputIterator output) {
                //
            }

            template <class InputIterator, class OutputIterator>
            static constexpr auto sample_after(InputIterator begin, InputIterator end, auto from, auto to, auto step, OutputIterator output) {
                //
            }
        };
    };
    
    template <class OutputIterator>
    concept container_output_iterator = requires {
        typename OutputIterator::container_type;
        typename OutputIterator::container_type::value_type;
    };

    template <class OutputIterator>
    struct iterator_value
    {
        using type = typename OutputIterator::value_type;
    };
    
    template <container_output_iterator OutputIterator>
    struct iterator_value<OutputIterator>
    {
        using type = typename OutputIterator::container_type::value_type;
    };

    template <class OutputIterator>
    using iterator_value_t = typename iterator_value<OutputIterator>::type;

    template <class Wrapping>
    struct no_smoothing_strategy
    {
        template <
            class InputIterator,
            class OutputIterator,

            class point_type = typename std::iterator_traits<InputIterator>::value_type,
            class anchor_type = iterator_value_t<OutputIterator>,
            class distance_type = typename anchor_type::distance_type
        >
            requires path_anchor<anchor_type>
        constexpr auto fill(InputIterator begin, InputIterator end, OutputIterator output, distance_type offset = distance_type{0}) const noexcept {
            *(output++) = anchor_type{*begin, offset};

            for (auto it = std::next(begin); it != end; ++begin, ++it) {
                auto & point = *it;
                *(output++) = anchor_type{point, offset += math::distance(*begin, point)};
            }

            return offset;
        }

        template <class InputIterator>
        constexpr auto interpolate(InputIterator begin, InputIterator end, auto t) const noexcept {
            BOOST_ASSERT_MSG(begin != end, "Empty range given");

            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);

            using point_type = typename anchor_type::point_type;

            auto it = lower_bound(begin, end, t);
                
            if (it == begin) {
                return Wrapping::before(begin, end, t);
            }

            if (it == end) {
                return Wrapping::after(begin, end, t);
            }
                
            auto prev = std::prev(it);
            return point_type{math::lerp(prev->position, it->position, (t - prev->offset) / (it->offset - prev->offset))};
        }
        
        template <class InputIterator>
        constexpr auto length(InputIterator begin, InputIterator end) const noexcept {
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);

            return Wrapping::length(begin, end);
        }

        template <class Path, class InputIterator, class Point>
            requires requires (Path v) {
                { v.emplace(v.begin(), std::declval<Point>(), 0) };
            }
        constexpr auto insert(Path & path, InputIterator where, Point && point) const noexcept {
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);
            
            using distance_type = typename anchor_type::distance_type;
            
            auto current_offset = [&] {
                if (where == path.begin()) {
                    return distance_type{0};
                }

                auto & prev = *std::prev(where);
                return distance_type{prev.offset + math::distance(prev.position, point)};
            } ();

            if (where != path.end()) {
                auto next_offset = current_offset + math::distance(point, where->position);
                auto delta = next_offset - where->offset;

                for (auto it = where; it != path.end(); ++it) {
                    it->offset += delta;
                }
            }

            where = path.emplace(where, std::forward<Point>(point), current_offset);
            return math::range{where, std::next(where)};
        }

        template <class Path, class InputIterator, class PointIterator>
            requires requires (Path v) {
                { v.emplace(v.begin(), *std::declval<PointIterator>(), 0) };
            }
        constexpr auto insert(Path & path, InputIterator where, PointIterator begin, PointIterator end) const noexcept {
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);
            
            using distance_type = typename anchor_type::distance_type;
            
            auto pos = std::distance(path.begin(), where);
            auto current_offset = [&]() {
                if (where == path.begin()) {
                    return distance_type{0};
                }

                auto & prev = *std::prev(where);
                return distance_type{prev.offset + math::distance(prev.position, *begin)};
            } ();
            
            fill(begin, end, std::inserter(path, where), current_offset);
            
            auto first = path.begin() + pos;
            auto last = first + std::distance(begin, end);

            if (last != path.end()) {
                auto delta = current_offset - last->offset;

                for (auto it = last; it != path.end(); ++it) {
                    it->offset += delta;
                }
            }

            return math::range{first, last};
        }

        template <class InputIterator, class OutputIterator>
        constexpr void select(InputIterator begin, InputIterator end, auto from, auto to, OutputIterator output) const noexcept {
            using output_value = iterator_value_t<OutputIterator>;
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);

            BOOST_ASSERT_MSG(begin != end, "Empty range given");

            auto it = lower_bound(begin, end, from);

            if (it == begin) {
                Wrapping::select_before(begin, end, from, to, output);
            }

            if (it->offset <= to) {
                if (it != begin && it->offset > from) {
                    auto & prev = *std::prev(it);
                    *(output++) = output_value(math::lerp(prev.position, it->position, (from - prev.offset) / (it->offset - prev.offset)));
                }
                
                for (; it != end && it->offset <= to; ++it) {
                    *(output++) = output_value(it->position);
                }
                
                if (it != end && it != begin) {
                    if (auto & prev = *std::prev(it); prev.offset < to) {
                        *(output++) = output_value(math::lerp(prev.position, it->position, (to - prev.offset) / (it->offset - prev.offset)));
                    }
                }
            }

            if (it == end) {
                Wrapping::select_after(begin, end, from, to, output);
            }
        }
        
        template <class InputIterator, class OutputIterator>
        constexpr void sample(InputIterator begin, InputIterator end, auto from, auto to, auto step, OutputIterator output) const noexcept {
            using output_value = iterator_value_t<OutputIterator>;
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);

            BOOST_ASSERT_MSG(begin != end, "Empty range given");

            auto it = lower_bound(begin, end, from); // fast search for the first sampled element

            if (it == begin) {
                Wrapping::sample_before(begin, end, from, to, step, output);
            }
            
            if (it != end) {
                auto offset = from + math::align(it->offset - from, step);
                it = std::max(it, std::next(begin));

                [&] {
                    for (; offset <= to; offset += step) {
                        while (it->offset < offset) {
                            if (++it == end) {
                                return;
                            }
                        }
                        
                        auto & current = *it;
                        auto & prev = *std::prev(it);
                        *(output++) = output_value(math::lerp(prev.position, current.position, (offset - prev.offset) / (current.offset - prev.offset)));
                    }
                } ();
            }

            if (it == end) {
                Wrapping::sample_after(begin, end, from, to, step, output);
            }
        }

        template <class InputIterator, class Predicate = std::less<>>
        constexpr auto lower_bound(InputIterator begin, InputIterator end, auto offset, Predicate && predicate = {}) const noexcept {
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);

            return std::lower_bound(begin, end, offset, [&](auto & anchor, auto offset) {
                return std::forward<Predicate>(predicate)(anchor.offset, offset);
            });
        }
        
        template <class InputIterator, class Predicate = std::less<>>
        constexpr auto upper_bound(InputIterator begin, InputIterator end, auto offset, Predicate && predicate = {}) const noexcept {
            using anchor_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(path_anchor<anchor_type>);

            return std::upper_bound(begin, end, offset, [&](auto offset, auto & anchor) {
                return std::forward<Predicate>(predicate)(offset, anchor.offset);
            });
        }

        template <class Range, class OutputIterator>
        constexpr auto fill(const Range & r, OutputIterator output) const noexcept {
            return fill(r.begin(), r.end(), output);
        }
        
        template <class Range>
        constexpr auto interpolate(const Range & r, auto t) const noexcept {
            return interpolate(r.begin(), r.end(), t);
        }
        
        template <class Range>
        constexpr auto length(const Range & r) const noexcept {
            return length(r.begin(), r.end());
        }

        template <class Range, class OutputIterator>
        constexpr void select(const Range & r, auto from, auto to, OutputIterator output) const noexcept {
            select(r.begin(), r.end(), from, to, output);
        }

        template <class Range, class OutputIterator>
        constexpr void sample(const Range & r, auto from, auto to, auto step, OutputIterator output) const noexcept {
            sample(r.begin(), r.end(), from, to, step, output);
        }
    };
}

//---------------------------------------------------------------------------
#endif
