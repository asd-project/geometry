#--------------------------------------------------------
#   package geometry_test
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

include(bootstrap)
include(module)

#--------------------------------------------------------

project(geometry_test VERSION 0.0.1)

#--------------------------------------------------------

module(TEST geometry_test)
    domain(app)

    group(src Sources)
    files(
        main.test.cpp
    )
endmodule()

#--------------------------------------------------------
