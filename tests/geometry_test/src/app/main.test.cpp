//---------------------------------------------------------------------------

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <launch/main.h>

#include <geometry/path.h>
#include <geometry/line_mesh.h>

#include <math/io.h>

//---------------------------------------------------------------------------

namespace asd::app
{
    template <auto epsilon>
    struct approx
    {
        using value_type = decltype(epsilon);
        
        constexpr approx() : value(0) {}
        constexpr approx(value_type value) : value(value) {}
        constexpr approx(const approx &) = default;
        constexpr approx & operator = (const approx &) = default;

        friend constexpr bool operator == (const approx & a, const approx & b) {
            return math::abs(a.value - b.value) < epsilon;
        }

        constexpr approx & operator += (const approx & v) {
            value += v.value;
            return *this;
        }

        constexpr approx & operator -= (const approx & v) {
            value -= v.value;
            return *this;
        }

        constexpr approx & operator *= (const approx & v) {
            value *= v.value;
            return *this;
        }

        constexpr approx & operator /= (const approx & v) {
            value /= v.value;
            return *this;
        }

        constexpr operator value_type () const {
            return value;
        }

        value_type value;
    };

    using approx_value = approx<1.0e-6f>;
    using approx_point = math::point<approx_value, math::point_model_xy>;

    TEST_CASE("static path", "[asd::geometry]") {
        constexpr auto points = std::array{
            approx_point{1.0f, 1.0f},
            approx_point{2.0f, 2.0f},
            approx_point{3.0f, 3.0f},
            approx_point{6.0f, 3.0f}
        };

        constexpr geom::no_smoothing_strategy<> sm;
        constexpr auto path = geom::make_path(points, sm);

        constexpr auto sqrt2 = math::sqrt(2.0f);
        
        static_assert(sm.length(path) == approx_value{2.0f * sqrt2 + 3.0f});

        static_assert(sm.interpolate(path, 0.0f) == approx_point{1.0f, 1.0f});
        static_assert(sm.interpolate(path, sqrt2) == approx_point{2.0f, 2.0f});
        static_assert(sm.interpolate(path, 2.0f * sqrt2) == approx_point{3.0f, 3.0f});
        static_assert(sm.interpolate(path, 2.0f * sqrt2 + 3.0f) == approx_point{6.0f, 3.0f});
        
        static_assert(sm.interpolate(path, 0.5f * sqrt2) == approx_point{1.5f, 1.5f});
        static_assert(sm.interpolate(path, 1.5f * sqrt2) == approx_point{2.5f, 2.5f});

        {
            constexpr auto selected_points = [&] {
                std::array<approx_point, 4> selected_points;
                sm.select(path, 0.0f, 6.0f, selected_points.begin()); // full select

                return selected_points;
            }();

            static_assert(math::eq(selected_points, {{{1.0f, 1.0f}, {2.0f, 2.0f}, {3.0f, 3.0f}, {6.0f, 3.0f}}}));
        }
        
        {
            constexpr auto selected_points = [&] {
                std::array<approx_point, 4> selected_points;
                sm.select(path, 0.0f, 1.5f * sqrt2, selected_points.begin()); // cut path end

                return selected_points;
            }();

            static_assert(math::eq(selected_points, {{{1.0f, 1.0f}, {2.0f, 2.0f}, {2.5f, 2.5f}}}));
        }
        
        {
            constexpr auto selected_points = [&] {
                std::array<approx_point, 3> selected_points;
                sm.select(path, 0.5f * sqrt2, 1.5f * sqrt2, selected_points.begin()); // cut both path ends

                return selected_points;
            }();

            static_assert(math::eq(selected_points, {{{1.5f, 1.5f}, {2.0f, 2.0f}, {2.5f, 2.5f}}}));
        }

        {
            constexpr auto selected_points = [&] {
                std::array<approx_point, 3> selected_points;
                sm.sample(path, 0.0f, 2.0f * sqrt2, sqrt2, selected_points.begin());

                return selected_points;
            }();

            static_assert(math::eq(selected_points, {{{1.0f, 1.0f}, {2.0f, 2.0f}, {3.0f, 3.0f}}}));
        }

        {
            constexpr auto selected_points = [&] {
                std::array<approx_point, 4> selected_points;
                sm.sample(path, 0.0f, 3.0f * sqrt2, sqrt2, selected_points.begin());

                return selected_points;
            }();

            static_assert(math::eq(selected_points, {{{1.0f, 1.0f}, {2.0f, 2.0f}, {3.0f, 3.0f}, {3.0f + sqrt2, 3.0f}}}));
        }
    }
    
    TEST_CASE("dynamic path", "[asd::geometry]") {
        constexpr auto points = std::array {
            approx_point{2.0f, 2.0f}
        };

        constexpr geom::no_smoothing_strategy<> sm;

        auto path = [&](const auto & points) {
            using anchor_type = geom::default_anchor<approx_point>;

            std::vector<anchor_type> anchors;
            sm.fill(points.begin(), points.end(), std::back_inserter(anchors));

            return anchors;
        }(points);

        constexpr approx_value sqrt2 = math::sqrt(2.0f);

        REQUIRE(sm.length(path) == approx_value{0.0f});
        REQUIRE(sm.interpolate(path, 0.0f) == approx_point{2.0f, 2.0f});
        
        sm.insert(path, path.begin(), approx_point{1.0f, 1.0f});
        
        REQUIRE(sm.length(path) == approx_value{sqrt2});
        REQUIRE(sm.interpolate(path, 0.0f) == approx_point{1.0f, 1.0f});

        constexpr auto new_points = std::array {
            approx_point{3.0f, 3.0f},
            approx_point{6.0f, 3.0f}
        };

        sm.insert(path, path.end(), new_points.begin(), new_points.end());
        
        REQUIRE(sm.length(path) == approx_value{2.0f * sqrt2 + 3.0f});

        REQUIRE(sm.interpolate(path, sqrt2) == approx_point{2.0f, 2.0f});
        REQUIRE(sm.interpolate(path, 2.0f * sqrt2) == approx_point{3.0f, 3.0f});
        REQUIRE(sm.interpolate(path, 2.0f * sqrt2 + 3.0f) == approx_point{6.0f, 3.0f});
        
        REQUIRE(sm.interpolate(path, 0.5f * sqrt2) == approx_point{1.5f, 1.5f});
        REQUIRE(sm.interpolate(path, 1.5f * sqrt2) == approx_point{2.5f, 2.5f});
        REQUIRE(sm.interpolate(path.begin() + 1, path.end(), 1.5f * sqrt2) == approx_point{2.5f, 2.5f});
        REQUIRE(sm.interpolate(path.begin() + 2, path.end(), 1.5f * sqrt2) == approx_point{3.0f, 3.0f});

        std::vector<approx_point> selected_points;
        sm.select(path, 0.0f, 6.0f, std::back_inserter(selected_points)); // full select set
        REQUIRE(math::eq(selected_points, {{1.0f, 1.0f}, {2.0f, 2.0f}, {3.0f, 3.0f}, {6.0f, 3.0f}}));

        selected_points.clear();
        sm.select(path, 0.0f, 1.5f * sqrt2, std::back_inserter(selected_points)); // cut path end
        REQUIRE(math::eq(selected_points, {{1.0f, 1.0f}, {2.0f, 2.0f}, {2.5f, 2.5f}}));

        selected_points.clear();
        sm.select(path, 0.5f * sqrt2, 1.5f * sqrt2, std::back_inserter(selected_points)); // cut both path ends
        REQUIRE(math::eq(selected_points, {{1.5f, 1.5f}, {2.0f, 2.0f}, {2.5f, 2.5f}}));
    }

    static ::asd::entrance open([](int argc, char * argv[]) {
        Catch::Session session;

        if (int return_code = session.applyCommandLine(argc, argv); return_code != 0) {
            return return_code;
        }

        return session.run();
    });
}

//---------------------------------------------------------------------------
